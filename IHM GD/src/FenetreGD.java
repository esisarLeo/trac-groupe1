import javax.swing.JFrame;

public class FenetreGD extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PanneauGD pan = new PanneauGD("Images/for�t.gif");

	public FenetreGD() {
		this.setTitle("Babar");
		this.setSize(1600, 900);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setContentPane(pan);
		this.setVisible(true);
		go();
	}

	private void go() {
		for (int i = 0; i < pan.getWidth(); i++) {
			for (int j = 0; j < pan.getHeight(); j++) {
				int x = pan.getPosX();
				x++;
				pan.setPosX(x);
				pan.repaint();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

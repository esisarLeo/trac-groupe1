package trac.text;

public class Word {

	private String word;
	private String imagePath;
	private String songPath;
	private boolean isFont;
	
	public Word(String word, String imagePath, String songPath) {
		super();
		this.word = word;
		this.imagePath = imagePath;
		this.songPath = songPath;
	}

	public String getWord() {
		return word;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getSongPath() {
		return songPath;
	}
}

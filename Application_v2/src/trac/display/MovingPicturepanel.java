package trac.display;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
 
public class MovingPicturepanel extends JPanel {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//conditions initiales n�gatives pour que l'image apparaisse depuis l'ext�rieur de l'�cran
	private int posX = 0;
	private int posY = 250;
	private int width = 1300;
	private int heigth = 1000;
	

	private String adresseImage;

  public MovingPicturepanel(String adImage) {
	  adresseImage=adImage;
	  
	  System.out.println("largeur"+this.getWidth()+"hauteur"+this.getHeight());
	  
	}

public void paintComponent(Graphics g){
	  
//Fond de la fen�tre
    //On choisit une couleur de fond pour le rectangle
    g.setColor(Color.WHITE);
    //On le dessine de sorte qu'il occupe toute la surface
    g.fillRect(0, 0, width, heigth);
    
    try {
        Image img = ImageIO.read(new File("./ressources/babar/Babar.png"));
        
        g.drawImage((new ImageIcon(adresseImage)).getImage(),0,0,width,heigth, this); //
        
        
        g.drawImage(img, posX, posY, this);
        
        
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }
}
package trac.display;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import trac.text.Word;

public class WordListPanel extends JPanel implements MouseListener {
	private Set<JPanel> wordPanelList = null;

	public WordListPanel(Set<Word> words) {
		super();
		this.wordPanelList = new HashSet<JPanel>();
		this.setBackground(Color.WHITE);

		// conversion de la liste de Word en liste de Jpanel
		for (Word word : words) {
			WordPanel wordPanel = new WordPanel(word);
			wordPanel.addMouseListener(this);
			wordPanel.setUnselectedAppearance();
			this.wordPanelList.add(wordPanel);

		}

		// definition du layout

		// BOXLAYOUT

		BoxLayout layout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
		this.setLayout(layout);

		// FLOWLAYOUT
//		FlowLayout layout = new FlowLayout();
//		this.setLayout(layout);		

		this.updatePanel();

	}

	public void updatePanel() {
		this.removeAll();
		for (JPanel wordPanel : wordPanelList) {
			this.add(Box.createRigidArea(new Dimension(0, 10)));
			this.add(wordPanel);
		}
		this.revalidate();
		this.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		WordPanel wordPanel = (WordPanel) arg0.getComponent();
		System.out.println(wordPanel.getWord().getWord() + " est selectionné");
		wordPanel.setSelectedAppearance();
		this.updatePanel();

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		WordPanel wordPanel = (WordPanel) arg0.getComponent();

		if (wordPanel.contains(arg0.getPoint()))
			return;

		wordPanel.setUnselectedAppearance();

		this.updatePanel();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public Set<JPanel> getWordPanelList() {
		return wordPanelList;
	}

	public void setWordPanelList(Set<Word> words) {
		// conversion de la liste de Word en liste de Jpanel
		this.wordPanelList.clear();
		for (Word word : words) {
			WordPanel wordPanel = new WordPanel(word);
			wordPanel.addMouseListener(this);
			wordPanel.setUnselectedAppearance();
			this.wordPanelList.add(wordPanel);
		}
	}

}

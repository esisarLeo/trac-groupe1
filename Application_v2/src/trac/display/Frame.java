package trac.display;

import java.awt.Color;
import java.awt.Container;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.esisar.trac.groupe1.Panel;
import trac.text.Word;

public class Frame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SelectionPanel selectionPan;
	private MovingPicturepanel graphicalPan;
	
	public Frame(String name,Set<Word> words) {
		super(name);

		this.setSize(1600,900);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
		
		selectionPan = new SelectionPanel(words);
		graphicalPan = new MovingPicturepanel("./ressources/foret/foret.gif");
		
		Container contentPane = this.getContentPane();
		contentPane.add(selectionPan);
		contentPane.add(graphicalPan);
		graphicalPan.repaint();
		System.out.println("largeur"+selectionPan.getWidth()+"hauteur"+selectionPan.getHeight());
		go();
	}
	
	private void go() {
		
		for (int i = 0; i < graphicalPan.getWidth(); i++) {
			for (int j = 0; j < graphicalPan.getHeight(); j++) {
				int x = graphicalPan.getPosX();
				x++;
				graphicalPan.setPosX(x);
				graphicalPan.repaint();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

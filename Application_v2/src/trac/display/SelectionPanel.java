package trac.display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import trac.text.Word;

public class SelectionPanel extends JPanel {

	private Set<Word> words = null;
	private SearchPanel searchPanel;
	private WordListPanel wordListPanel;

	public SelectionPanel(Set<Word> words) {
		super();
		this.words = words;

		// initialisation des composants graphiques
		this.setSize(300, 1000);

		this.setLayout(new BorderLayout());


		searchPanel = new SearchPanel();
		searchPanel.getSearchField().getDocument().addDocumentListener(new SearchFieldListener());
		wordListPanel = new WordListPanel(words);

		this.add(searchPanel, BorderLayout.NORTH);
		this.add(wordListPanel, BorderLayout.CENTER);
	}

	class SearchFieldListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub
			update();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			// TODO Auto-generated method stub
			update();
		}

		public void update() {
			String searchString = searchPanel.getSearchField().getText();
			System.out.println("Recherche : " + searchString);

			// mise a jour de la selection
			Set<Word> selectedWords = new HashSet<Word>();
			for (Word word : words) {
				if (word.getWord().startsWith(searchString)) {
					System.out.println(word.getWord());
					selectedWords.add(word);
				}
			}
			//mise a jour du composant WordListPanel
			wordListPanel.setWordPanelList(selectedWords);
			wordListPanel.updatePanel();
		}

	}

//	class SearchButtonListener implements ActionListener{
//		@Override
//		public void actionPerformed(ActionEvent arg0) {
//			String searchString = searchPanel.getSearchField().getText();
//			System.out.println("Recherche : "+searchString);
//			
//			//mise a jour de la selection
//			Set<Word> selectedWords = new HashSet<Word>();
//			for(Word word:words) {
//				if(word.getWord().startsWith(searchString)) {
//					System.out.println(word.getWord());
//					selectedWords.add(word);
//				}
//			}
//			
//			//mise a jour du composant WordListPanel
//			wordListPanel.setWordPanelList(selectedWords);
//			wordListPanel.updatePanel();			
//		}
//		
//	}

}

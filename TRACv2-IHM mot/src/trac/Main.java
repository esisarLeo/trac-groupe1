package trac;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;

import trac.audio.Player;
import trac.display.Display;
import trac.text.Word;

public class Main {

	public static void main(String[] args) {
		
		Set<Word> words = new HashSet<Word>();
		words.add(new Word("foret","./ressources/foret/icon_foret.jpg","./ressources/foret/foret.wav"));
		words.add(new Word("voiture","",""));
		words.add(new Word("babar","",""));
		words.add(new Word("ville","./ressources/ville/icon_ville.jpg","./ressources/ville/ville.wav"));
		words.add(new Word("elephant","",""));
		words.add(new Word("auto","",""));
		words.add(new Word("celeste","",""));
		words.add(new Word("singe","",""));
		
		Display ihm = new Display(words);
		System.out.println("===============================");
		System.out.println("========== mots Babar =========");
		System.out.println("===============================");

		
	}
}

# TRAC groupe1

Ce document peut servir à donner des informations pratiques sur l'utilisation de ce git et à donner l'avancée du projet date par date.

## Avancée :

11/11/2020 : lecture d'un fichier audio grâce à un programme Java (extensions prises en charge : Wave, Aiff...).

12/11/2020 : création du cadre de l'interface graphique avec Java Awt et résultats actuels :
- fenêtre avec fond coloré où imagé >>classe IHM
- fenêtre avec image mobile (extensions possibles : png, jpg...) >>classe IHM dynamique
- fenêtre avec fond animé (utilisation d'un gif en fond d'interface) >>classe IHM Gif

15/11/2020 : test de gitlab
- Mise en place de gitlab

16/11/2020 :  
- création d'un projet java (TRACv2-IHM mot) qui a pour but de gerer la liste de mots selectionnés.
- definition des Layout et de deux parties distinctes (barre de recherche et liste de mots)

18/11/2020 : finalisation de la présentation générale de l'interface pour la séance du 19 novembre.
>>Application_v4

19/11/2020 : changement de la partie gauche de l'interface pour permettre la composition d'un texte

22/11/2020 : 
- mise en place d'un code propre
- création d'une classe ImageMobile pour gérer chaque objet png ajouté à l'interface

26/11/2020 :
- Création d'un bouton permettant d'ajouter des mots personnalisés
- Le bouton terminer permet d'effacer completement la scène
- Création d'un executable utilisable sans java
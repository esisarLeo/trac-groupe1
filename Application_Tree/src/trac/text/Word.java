package trac.text;

/*
 * Cette classe permet d'avoir toutes les infos des mots declencheurs (chemin vers son et image)
 */
public class Word {

	private String word;
	private String imagePath;
	private String songPath;
	private boolean isBackground;
	
	public Word(String word, String imagePath, String songPath,boolean isBackground) {
		super();
		this.word = word;
		this.imagePath = imagePath;
		this.songPath = songPath;
		this.isBackground = isBackground;
	}

	public String getWord() {
		return word;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getSongPath() {
		return songPath;
	}
	
	public boolean isBackground() {
		return isBackground;
	}
}

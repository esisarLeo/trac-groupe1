package trac;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.PrintJob;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dialog.ModalityType;
import java.awt.datatransfer.Clipboard;
import java.awt.font.TextAttribute;
import java.awt.im.InputMethodHighlight;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import java.awt.Toolkit;

import trac.display.Frame;
import trac.text.Word;

public class Application {

	public static void main(String[] args) {
		//construction de la liste de mot et association des images et des ressources
		Set<Word> words = new HashSet<Word>();
		words.add(new Word("foret","./ressources/foret/foret.gif","./ressources/foret/foret.wav",true));
		words.add(new Word("voiture","./ressources/voiture/voiture.png","",false));
		words.add(new Word("babar","./ressources/babar/babar.png","./ressources/babar/babar.wav",false));
		words.add(new Word("ville","./ressources/ville/ville.jpg","./ressources/ville/ville.wav",true));
		words.add(new Word("elephants","./ressources/elephants/elephants.png","./ressources/elephants/elephants.wav",false));
		words.add(new Word("auto","","",false));
		words.add(new Word("celeste","","",false));
		words.add(new Word("singe","","",false));
		words.add(new Word("chasseur","./ressources/chasseur/chasseur.png","./ressources/chasseur/chasseur.wav",false));
		
		//creation de la frame
		
	    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Frame frame1 = new Frame(words,(int)screenSize.getWidth(),(int)screenSize.getHeight());
	}
}

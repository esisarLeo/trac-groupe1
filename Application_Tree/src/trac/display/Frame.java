package trac.display;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

import javax.swing.text.BadLocationException;

import trac.display.graphical.GraphicalPanel;
//import trac.display.graphical.ImageMobile;
import trac.display.textEditor.Composer;
import trac.display.textEditor.CompositionPanel;
import trac.display.textEditor.EditorPanel;
import trac.display.textEditor.TreePanel;
import trac.text.Word;

/**
 * Cette classe contruit la fenetre principale et place les differents composants (JPanel) dans cette même fenetre
 */

public class Frame extends JFrame{

	
	private static final long serialVersionUID = 1L;
	
	private int width;
	private int height;
	
	private CompositionPanel compositionPan;
	private GraphicalPanel graphicalPan;
	//private TreePanel tree;
	
	private Set<Word> words;

	public Frame(Set<Word> words,int width,int height) {
		super("TRAC gr1 : Presentation IHM (Beta)");
		
		this.setSize(width,height);
		this.setResizable(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.words = words;
		
		//création du panneau de composition
		compositionPan = new CompositionPanel(words,new Point(0, 0),new Dimension(400, this.getHeight()));
		compositionPan.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5,Color.LIGHT_GRAY));
		compositionPan.getButtonPanel().getEndLineButton().addActionListener(new EndLineButtonListener());
		
		//creation du panneau d'affichage graphique
		graphicalPan = new GraphicalPanel(new Point(400, 0),new Dimension(this.getWidth()-400, this.getHeight()));
	
		//tree = new TreePanel("./ressources/", new Point(0,0), new Dimension(400,this.getHeight()));
		
		Container contentPane = this.getContentPane();
		contentPane.setLayout(null);//pas de layout manager pour les 2 Panel, on les place directement a partir des coordonées
		contentPane.add(compositionPan);
		contentPane.add(graphicalPan);
		
		//graphicalPan.go();
//		graphicalPan.setBackGround("ressources/foret/foret.gif");
//		graphicalPan.addImage(new ImageMobile("babar1","ressources/babar/Babar.png", 0, 250));
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		graphicalPan.addImage(new ImageMobile("babar2","ressources/babar/Babar.png", 250, 250));
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		graphicalPan.addImage(new ImageMobile("babar3","ressources/babar/Babar.png", 500, 250));
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		graphicalPan.removeImage("babar2");
		this.setVisible(true);
		
	}
	
	//classe qui se declenche a l'appui sur le bouton
	class EndLineButtonListener implements ActionListener{
		String sentence;
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
						int off;
			try {
				off = compositionPan.getEditorPanel().getTextEditor().getLineStartOffset(compositionPan.getEditorPanel().getTextEditor().getLineCount()-1);
				int end = compositionPan.getEditorPanel().getTextEditor().getLineEndOffset(compositionPan.getEditorPanel().getTextEditor().getLineCount()-1);

				sentence = compositionPan.getEditorPanel().getTextEditor().getText(off,end-off);	
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//lancement du thread de composition
			new Composer(sentence, compositionPan.getSentencePanel(),graphicalPan,words).start();
		}	
	}
	
}

package trac.display.graphical;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/* Image mobile va permettre cr�er l'image qu'on veut afficher directement 
 * dans l'interface graphique, et lui attribue une position dans celle-ci, 
 * si l'image charg�e est trop grande, cette classe r�duit sa taille
 */

public class ImageMobile {
	private int posX;
	private int posY;
	private String name;
	private BufferedImage img;

	public ImageMobile(String name, String adresseImage) {
		try {
			img = ImageIO.read(new File(adresseImage));
			img = resize(img);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.name = name;
		this.posX = 0;
		this.posY = 0;
	}
	
	public void setLocation(int posX,int posY) {
		this.posX = posX;
		this.posY = posY;
	}

	private BufferedImage resize(BufferedImage img) {
		System.out.println("largeur"+img.getWidth());
		int largeur = img.getWidth(null);
		int hauteur = img.getHeight(null);
		BufferedImage resizedImage;

		if (largeur > 400 || hauteur > 400) {
			if (largeur > hauteur) {
				hauteur = hauteur * 400/largeur;
				largeur = 400;
			} else {
				largeur = largeur * 400/hauteur;
				hauteur = 400;
			}

			// creates blank output image
			resizedImage = new BufferedImage(largeur, hauteur, img.getType());

			// fill output image
			Graphics2D g2d = resizedImage.createGraphics();
			g2d.drawImage(img, 0, 0,largeur, hauteur, null);
			g2d.dispose();
		}else {
			resizedImage=img;
		}

		return resizedImage;

	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public Image getImg() {
		return img;
	}

	public void setImg(BufferedImage img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

}

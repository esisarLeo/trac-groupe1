package trac.display.graphical;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Cette classe crée le panel de la zone graphique
 */

public class GraphicalPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private List<ImageMobile> images;
	
	private String adresseImage;


	// fond neutre
	public GraphicalPanel(Point point, Dimension dimension){
		super();
		
		images = new ArrayList<ImageMobile>();
		this.adresseImage =null ;

		this.setSize(dimension);
		this.setLocation(point);
		
		System.out.println("adresseImage="+adresseImage);

	}

	// fond avec image/gif
	public GraphicalPanel(String adImage, Point point, Dimension dimension) {
		super();
		
		this.adresseImage = adImage;
		
		images = new ArrayList<ImageMobile>();
		
		this.setSize(dimension);
		this.setLocation(point);
	}

	public void setBackGround(String adImage) {
		if(new File(adImage).exists()) {
			this.adresseImage = adImage;
			this.revalidate();
			this.repaint();
		}else{
			System.out.println("Cette image n'existe pas "+adImage);
		}
		
	}

	public void paintComponent(Graphics g) {

		// Fond de la fenetre
		if (adresseImage == null) {
			// On choisit une couleur de fond pour le rectangle
			g.setColor(Color.WHITE);
			// On le dessine de sorte qu'il occupe toute la surface
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
		} else {
			g.drawImage((new ImageIcon(adresseImage)).getImage(), 0, 0, this.getWidth(), this.getHeight(), this); // arri�re
																												// //
																													// plan
		}
		
		for(int i=0; i<images.size();i++) {
			try{
				g.drawImage(images.get(i).getImg(), images.get(i).getPosX(), images.get(i).getPosY(), this); // image png par dessus
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	// fonction de gestion de la position du png

//	public void go() {
//
//		for (int i = 0; i < this.getWidth(); i++) {
//			int x = this.getPosX();
//			x++;
//			this.setPosX(x);
//			this.revalidate();
//			this.repaint();
//
//			try {
//				Thread.sleep(10);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//
//		}
//	}
	
	//ajout des diff�rentes images png � notre liste
	public void addImage(ImageMobile imgM) {
		boolean alreadyExist = false;
		for(ImageMobile img:images) {
			if(img.getName().equals(imgM.getName())){
				System.out.println("Ce nom ("+imgM.getName()+") est deja utilis� !");
				alreadyExist = true;
			}
		}
		
		if(!alreadyExist) {
			images.add(imgM);
			this.revalidate();
			this.repaint();
		}		
	}

	//suppression des diff�rentes images png de notre liste
	public void removeImage(String name) {
		for(ImageMobile img:images) {
			if(img.getName().equals(name)){
				images.remove(img);
				this.revalidate();
				this.repaint();
			}
		}		
	}
	
	//suppression de toutes les images png de notre liste
		public void clearImage() {
			images.clear();
			this.revalidate();
			this.repaint();
		}
}
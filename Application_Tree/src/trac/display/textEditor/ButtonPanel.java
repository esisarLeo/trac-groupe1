package trac.display.textEditor;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;

/*
 * Cette classe construit les boutons en dessous de l'editeur de texte et est appelée par la classe EditorPanel
 */
public class ButtonPanel extends JPanel{
	private JButton endLineButton;
	private JButton endTextButton;

	public ButtonPanel() {
		super();
		this.endLineButton = new JButton("Composer");
		this.endTextButton = new JButton("Terminer");
		
		this.add(endLineButton);
		this.add(endTextButton);
	}

	public JButton getEndLineButton() {
		return endLineButton;
	}

	public void setEndLineButton(JButton endLineButton) {
		this.endLineButton = endLineButton;
	}

	public JButton getEndTextButton() {
		return endTextButton;
	}

	public void setEndTextButton(JButton endTextButton) {
		this.endTextButton = endTextButton;
	}
	
	
}

package trac;

import java.util.HashSet;
import java.util.Set;

import trac.display.Frame;
import trac.text.Word;

public class Application {

	public static void main(String[] args) {
		//construction de la liste de mot
		Set<Word> words = new HashSet<Word>();
		words.add(new Word("foret","./ressources/foret/icon_foret.jpg","./ressources/foret/foret.wav"));
		words.add(new Word("voiture","",""));
		words.add(new Word("babar","",""));
		words.add(new Word("ville","./ressources/ville/icon_ville.jpg","./ressources/ville/ville.wav"));
		words.add(new Word("elephant","",""));
		words.add(new Word("auto","",""));
		words.add(new Word("celeste","",""));
		words.add(new Word("singe","",""));
		
		Frame frame1 = new Frame("TRAC gr1 : Presentation IHM (Beta)",words);
		frame1.setVisible(true);
	}
}

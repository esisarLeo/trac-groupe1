package trac.display.selection;

import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchPanel extends JPanel{
	private JTextField searchField = null;
	//private JButton searchButton = null;
	
	public SearchPanel() {
		searchField = new JTextField(15);
		searchField.setFont(new Font(Font.SANS_SERIF,Font.LAYOUT_LEFT_TO_RIGHT,20));
		//searchButton = new JButton("Rechercher");
		
		this.add(searchField);
		//this.add(searchButton);
	}

	public JTextField getSearchField() {
		return searchField;
	}
//	public JButton getSearchButton() {
//		return searchButton;
//	}	
}

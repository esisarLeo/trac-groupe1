package trac.display.selection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import trac.audio.Player;
import trac.text.Word;

public class WordPanel extends JPanel{
	
	private Word word;
	private JLabel wordLabel;
	private JLabel imageLabel;
	private JButton imageButton;
	private JButton songButton;
	private boolean isImagePresent;

	public WordPanel(Word word) {
		super();
		this.word = word;

		// définition du JLabel mot
		this.wordLabel = new JLabel(word.getWord());

		this.wordLabel.setMaximumSize(new Dimension(250, 50));
		this.wordLabel.setHorizontalAlignment(SwingConstants.CENTER);

		// définition du JLabel image
		if(new File(word.getImagePath()).exists()) {
			this.imageLabel = new JLabel(new ImageIcon(word.getImagePath()));
			isImagePresent = true;
		}	else {
			isImagePresent = false;
		}

		// definition des boutons
		this.imageButton = new JButton(new ImageIcon("./ressources/icones/icon_image.png"));
		this.songButton = new JButton(new ImageIcon("./ressources/icones/icon_music.png"));
		this.songButton.addActionListener(new MusicBoutonListener(word));
		this.imageButton.addActionListener(new ImageBoutonListener(word));
		// definition du layout du Panel
		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));

		this.setBackground(Color.LIGHT_GRAY);
		this.setBorder(BorderFactory.createMatteBorder(1, 5, 1, 1, Color.black));
		this.setOpaque(true);
	}
	
	public void setSelectedAppearance() {
		this.removeAll();
		this.add(Box.createRigidArea(new Dimension(5, 0)));
		this.add(wordLabel);
		this.add(Box.createRigidArea(new Dimension(5, 0)));
		if(isImagePresent) {
			this.add(imageLabel);
			this.add(Box.createRigidArea(new Dimension(5, 0)));
		}
		this.add(imageButton);
		this.add(Box.createRigidArea(new Dimension(5, 0)));
		this.add(songButton);
		this.add(Box.createRigidArea(new Dimension(5, 0)));
		this.revalidate();
		this.repaint();
	}
	
	public void setUnselectedAppearance() {		
		this.removeAll();
		this.add(wordLabel);
		this.revalidate();
		this.repaint();
	}

	public Word getWord() {
		return word;
	}
	
	class MusicBoutonListener implements ActionListener{
		private Word word;
		
		public MusicBoutonListener(Word word) {
			// TODO Auto-generated constructor stub
			this.word = word;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Player player = new Player(word.getSongPath());
			player.start();
			
		}
	}
	
	class ImageBoutonListener implements ActionListener{
		private Word word;
		
		public ImageBoutonListener(Word word) {
			// TODO Auto-generated constructor stub
			this.word = word;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("appui sur le bouton image: "+word.getWord());			
		}
	}
}

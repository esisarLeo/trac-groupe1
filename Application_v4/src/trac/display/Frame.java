package trac.display;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

import trac.display.graphical.GraphicalPanel;
import trac.display.selection.SelectionPanel;
import trac.text.Word;

public class Frame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SelectionPanel selectionPan;
	private GraphicalPanel graphicalPan;
	
	public Frame(String name,Set<Word> words) {
		super(name);

		this.setSize(1600,900);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		selectionPan = new SelectionPanel(words,new Point(0, 0),new Dimension(400, 1000));
		selectionPan.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5,Color.LIGHT_GRAY));
		
		graphicalPan = new GraphicalPanel("./ressources/foret/foret.gif",new Point(400, 0),new Dimension(1200, 1000));

		Container contentPane = this.getContentPane();
		contentPane.setLayout(null);//pas de layout manager pour les 2 Panel, on les place directement a partir des coordonées
		contentPane.add(selectionPan);
		contentPane.add(graphicalPan);
		go();
	}
	
	private void go() {
		
		for (int i = 0; i < 300; i++) {
			for (int j = 0; j < 1000; j++) {
				int x = graphicalPan.getPosX();
				x++;
				graphicalPan.setPosX(x);
				graphicalPan.repaint();
				
				this.revalidate();
				this.repaint();
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

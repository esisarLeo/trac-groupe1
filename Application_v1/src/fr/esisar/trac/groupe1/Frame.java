package fr.esisar.trac.groupe1;

import javax.swing.JFrame;

public class Frame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Panel pan;
	
	public Frame() {
		super("Nom_Application");
		set_prop();
	}
	
	private void set_prop() {
		this.setSize(1600,1000);
		this.setResizable(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		pan = new Panel();
		this.setContentPane(pan);
	}
}

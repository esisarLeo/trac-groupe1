import java.awt.Color;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
 
public class PanneauGif extends JPanel { 
	
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String adresseImage;
	 
	public PanneauGif(String adImage)
    {
        adresseImage=adImage;
    }
    

public void paintComponent(Graphics g){

    // efface le contenu précédent : 
    g.setColor(Color.white);
    g.fillRect(0, 0, this.getWidth(), this.getHeight());
    // dessine le gif : 
    g.drawImage((new ImageIcon(adresseImage)).getImage(),0,0,1920,1080, this);              
	}
}
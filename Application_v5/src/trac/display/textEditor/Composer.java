package trac.display.textEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import trac.audio.Player;
import trac.display.graphical.GraphicalPanel;
import trac.display.graphical.ImageMobile;
import trac.text.Word;

/*
 * Cette classe permet de composer la phrase passsée en parametre cad:
 * -ecrire la phrase progressivement dans la zone de texte "phrase a composer"
 * -detecte les mots declencheur et execute un son ou/et affiche une image en même temps
 */
public class Composer extends Thread {
	String sentence;
	SentencePanel sentencePanel;
	GraphicalPanel graphicalPanel;
	Set<Word> words;
	
	public Composer(String sentence,SentencePanel sentencePanel,GraphicalPanel graphicalPanel,Set<Word> words) {
		this.sentence = sentence;
		this.sentencePanel = sentencePanel;
		this.graphicalPanel = graphicalPanel;
		this.words = words;
	}
	
	public void run() {
		String displaySentence = "";
		Word triggerWord;
		for(String word:getWords(sentence)) {
			triggerWord = getTriggerWord(word);
			if(triggerWord == null) {
				displaySentence += word + " ";
			}else {
				displaySentence += "<span bgcolor=\"yellow\">" + word +"</span> ";
				if(triggerWord.getImagePath()!=null) {
					if(triggerWord.isBackground()) {
						graphicalPanel.setBackGround(triggerWord.getImagePath());
					}else {
						ImageMobile imgMobile = new ImageMobile(word, triggerWord.getImagePath());
						int posX = getRandomNumberInRange(0,graphicalPanel.getWidth()-imgMobile.getImg().getWidth(null));
						int posY = getRandomNumberInRange(0,graphicalPanel.getHeight()-imgMobile.getImg().getHeight(null));
						imgMobile.setLocation(posX, posY);
						graphicalPanel.addImage(imgMobile);
					}
				}
				
				if(triggerWord.getSongPath() != null) {
					new Player(triggerWord.getSongPath()).start();
				}				
			}
			
			sentencePanel.getSentenceLabel().setText("<html>"+displaySentence+"</html>");
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//fonction qui decoupe la phrase en un e liste de mots
	private List<String> getWords(String sentence){
		
		List<String> words = new ArrayList<String>();
		String word;
		boolean end = false;
		
		while( end == false ){
			if(sentence.indexOf(" ") != -1) {
				word = sentence.substring(0, sentence.indexOf(" "));
				sentence = sentence.substring(sentence.indexOf(" ")+1);
			}else {
				word = sentence;
				end = true;
			}
			words.add(word);
		}	
		return words;
	}
	
	//fonction qui retourne le mot declencheur associ� ou null si pas de mot declencheur associ�
	private Word getTriggerWord(String strWord) {
		Word triggerWord = null;
		for(Word word:words) {
			if(word.getWord().equalsIgnoreCase(strWord)){
				triggerWord = word;
			}
		}
		return triggerWord;
	}
	
	//genere un nombre al�atoire entre les bornes selectionn�es
	private int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

	public void setWords(Set<Word> words) {
		this.words = words;
	}
}

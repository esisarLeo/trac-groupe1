package trac.display.textEditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/*
 * Cette classe construit l'editeur de texte et les 2 boutons de composition
 */
public class EditorPanel extends JPanel{
	private JTextArea textEditor;
	private int lastRead;

	public EditorPanel() {
		super();
		this.textEditor = new JTextArea();
		this.textEditor.setPreferredSize(new Dimension(395, 500));
		this.textEditor.setFont(new Font(Font.SANS_SERIF,Font.LAYOUT_LEFT_TO_RIGHT,15));
		this.textEditor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2),"Tapez votre histoire ici ..."));
				
		this.lastRead = 0;
				
		this.add(textEditor);
	}

	public JTextArea getTextEditor() {
		return textEditor;
	}

	public void setTextEditor(JTextArea textEditor) {
		this.textEditor = textEditor;
	}

	public int getLastRead() {
		return lastRead;
	}

	public void setLastRead(int lastRead) {
		this.lastRead = lastRead;
	}	
	
}

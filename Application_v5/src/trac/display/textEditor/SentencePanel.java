package trac.display.textEditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Cette classe construit le panel contenant le label "phrase en composition"
 */
public class SentencePanel extends JPanel{
	private JLabel sentenceLabel;

	public SentencePanel() {
		super();
		this.sentenceLabel = new JLabel();
		this.sentenceLabel.setPreferredSize(new Dimension(395, 40));
		this.sentenceLabel.setFont(new Font(Font.SANS_SERIF,Font.LAYOUT_LEFT_TO_RIGHT,15));
		this.sentenceLabel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2),"Phrase a composer"));
		
		this.add(sentenceLabel);
	}

	public JLabel getSentenceLabel() {
		return sentenceLabel;
	}

	public void setSentenceLabel(JLabel sentenceLabel) {
		this.sentenceLabel = sentenceLabel;
	}
	
	
	
}

package trac.display.textEditor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Set;

import javax.swing.JPanel;

import trac.text.Word;

/*
 *Cette classe construit tout le panel de composition (editeur de texte, label "phrase en composition", boutons) 
 */
public class CompositionPanel extends JPanel {

	private EditorPanel editorPanel;
	private SentencePanel sentencePanel;
	private ButtonPanel buttonPanel;
	private TreePanel treePanel;

	public CompositionPanel(Set<Word> words,Point origin,Dimension size) {
		super();
		
		this.setSize(size);
		this.setLocation(origin);
		
		// initialisation des composants graphiques
		editorPanel = new EditorPanel();		
		sentencePanel = new SentencePanel();
		treePanel = new TreePanel("ressources");
		buttonPanel = new ButtonPanel();
				
		this.add(sentencePanel);
		this.add(editorPanel);
		this.add(buttonPanel);
		this.add(treePanel);
	}

	public EditorPanel getEditorPanel() {
		return editorPanel;
	}

	public SentencePanel getSentencePanel() {
		return sentencePanel;
	}
	
	public ButtonPanel getButtonPanel() {
		return buttonPanel;
	}

	public void setButtonPanel(ButtonPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}
}

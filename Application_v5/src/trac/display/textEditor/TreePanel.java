package trac.display.textEditor;

//import java.io.File;

//import javax.swing.JPanel;
//import javax.swing.JTree;
//import javax.swing.tree.DefaultMutableTreeNode;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.io.*;

public class TreePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	JTree tree;
	DefaultMutableTreeNode root;
	
	public TreePanel(String add_ressources){
		root = new DefaultMutableTreeNode("root",true);
		getList(root, new File(add_ressources));
		//setLayout(new BorderLayout());
	    tree = new JTree(root);
	    tree.setRootVisible(false);
	    
	    tree.setPreferredSize(new Dimension(400,200));
	    
	    this.tree.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2),"Mots disponibles"));
		
		//this.add(new JScrollPane(tree));
	    this.add(tree);

		
	}

	  
	  public void getList(DefaultMutableTreeNode node, File f) {
		     if(!f.isDirectory()) {
		         if (f.getName().endsWith("gif") || f.getName().endsWith("jpg") ||  f.getName().endsWith("jpeg") || f.getName().endsWith("png")) {
		            System.out.println("IMAGE  -  " + f.getName());
		            DefaultMutableTreeNode child = new DefaultMutableTreeNode(f.getName());
		            node.add(child);
		            }
			     else if(f.getName().endsWith("wav")){
			            System.out.println("SON  -  " + f.getName());
			            DefaultMutableTreeNode child = new DefaultMutableTreeNode(f.getName());
			            node.add(child);
			     	}
		         }

		     else {
		    	 if(f.getName().contains("ressources")){
			         System.out.println("RACINE  -  " + f.getName());
			         DefaultMutableTreeNode child = new DefaultMutableTreeNode(f.getName());
			         node.add(child);
			         File fList[] = f.listFiles();
			         for(int i = 0; i  < fList.length; i++)
			             getList(child, fList[i]);
			         } 
		    	 else{
			         System.out.println("MOT  -  " + f.getName());
			         DefaultMutableTreeNode child = new DefaultMutableTreeNode(f.getName());
			         node.add(child);
			         File fList[] = f.listFiles();
			         for(int i = 0; i  < fList.length; i++)
			             getList(child, fList[i]);
			         } 
		    	 }
		    }
}

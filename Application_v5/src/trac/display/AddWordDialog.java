package trac.display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import trac.text.Word;

public class AddWordDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7955939079069424240L;

	private Set<Word> words;

	// attributs de notre fen�tre pop-up
	private JLabel nomLabel;
	private JTextField nomTextField;
	private JButton imageButton, sonButton;
	private JCheckBox checkBackground;
	private JFileChooser chooser;
	private String songPath, imagePath, songExtension, imageExtension,nom;

	public AddWordDialog(JFrame parent, boolean modal, Set<Word> words) {
		// On appelle le constructeur de JDialog correspondant
		super(parent, "Ajout d'un mot", modal);

		this.words = words;
		// On sp�cifie une taille
		this.setSize(450, 220);
		// La position
		this.setLocationRelativeTo(null);
		// La bo�te ne devra pas �tre redimensionnable
		this.setResizable(false);
		initComponent();
		// Enfin on l'affiche
		this.setVisible(true);
	}

	private void initComponent() {
		songPath = null;
		imagePath = null;

		// nom du mot
		nomLabel = new JLabel("Saisir un mot :");
		nomTextField = new JTextField();
		nomTextField.setPreferredSize(new Dimension(100, 25));
		JPanel panNom = new JPanel();
		panNom.setBackground(Color.white);
		panNom.setBorder(BorderFactory.createTitledBorder("Mot choisi"));
		panNom.add(nomLabel);
		panNom.add(nomTextField);

		// image
		imageButton = new JButton("Choix image");
		imageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("png, gif, jpg", "jpg", "gif", "png"));
				if (chooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
					imagePath = chooser.getSelectedFile().getPath();
					imageExtension = chooser.getSelectedFile().getName().substring(chooser.getSelectedFile().getName().indexOf("."));
				}
			}
		});
		checkBackground = new JCheckBox("image de fond");
		JPanel panImage = new JPanel();
		panImage.setBackground(Color.white);
		panImage.setBorder(BorderFactory.createTitledBorder("Image (.png, .jpg, .gif)"));
		panImage.add(imageButton);
		panImage.add(checkBackground);

		// Son
		sonButton = new JButton("Choix son");
		sonButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("wav", "wav"));
				if (chooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
					songPath = chooser.getSelectedFile().getPath();
					songExtension = chooser.getSelectedFile().getName().substring(chooser.getSelectedFile().getName().indexOf("."));
				}
			}
		});
		JPanel panSon = new JPanel();
		panSon.setBackground(Color.white);
		panSon.setBorder(BorderFactory.createTitledBorder("Son (.wav)"));
		panSon.add(sonButton);

		// Panel global
		JPanel content = new JPanel();
		content.setBackground(Color.white);
		content.add(panNom);
		content.add(panImage);
		content.add(panSon);

		// Panel de contr�le
		JPanel control = new JPanel();
		JButton okButton = new JButton("OK");

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean wordAlreadyExist = false;
				for (Word word : words) {
					if (word.getWord().equalsIgnoreCase(nomTextField.getText())) {
						wordAlreadyExist = true;
					}
				}
				
				JOptionPane errorPopup = new JOptionPane();
				if (!nomTextField.getText().isBlank() && !wordAlreadyExist && !(songPath == null && imagePath == null)) {
					nom = nomTextField.getText();
					createWordDirectory(nomTextField.getText(), checkBackground.isSelected());
				} else {
					if(wordAlreadyExist){
						JOptionPane.showMessageDialog(AddWordDialog.this, "Ce mot existe deja !", "Erreur", JOptionPane.ERROR_MESSAGE);
					} else if (nomTextField.getText().isBlank()) {
						JOptionPane.showMessageDialog(AddWordDialog.this, "Mot invalide !", "Erreur", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(AddWordDialog.this, "Il faut au moins une image ou un son !", "Erreur",JOptionPane.ERROR_MESSAGE);
					}
				}
				
				
				setVisible(false);
			}
		});

		JButton cancelButton = new JButton("Annuler");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});

		control.add(okButton);
		control.add(cancelButton);

		this.getContentPane().add(content, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);
	}

	private void createWordDirectory(String mot, boolean isBackground) {
		String wordDirectoryPath = "ressources/" + mot + "/";
		// cr�ation du repertoire du mot
		File wordDirectory = new File(wordDirectoryPath);
		if (!wordDirectory.exists()) {
			wordDirectory.mkdirs();
		}

		// ajout du fichier attribut
		File attributFile = new File(wordDirectoryPath + "attribut.txt");
		try {
			attributFile.createNewFile();
			FileWriter writer = new FileWriter(attributFile);
			if (isBackground) {
				writer.write("isBackground=true");
			} else {
				writer.write("isBackground=false");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// copie de l'image
		if (imagePath != null) {
			copyFile(wordDirectoryPath + nom + imageExtension, imagePath);
		}

		// copie du son
		if (songPath != null) {
			copyFile(wordDirectoryPath + nom + songExtension, songPath);
		}
	}

	private void copyFile(String destPath, String sourcePath) {
		try {
			FileInputStream sourceFileStream = new FileInputStream(new File(sourcePath));
			FileOutputStream destFileStream = new FileOutputStream(new File(destPath));

			byte[] buf = new byte[4096];
			int len = sourceFileStream.read(buf);
			while (len != -1) {
				destFileStream.write(buf, 0, len);
				len = sourceFileStream.read(buf);
			}

			sourceFileStream.close();
			destFileStream.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
package trac.display;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.text.BadLocationException;

import trac.audio.Player;
import trac.display.graphical.GraphicalPanel;
import trac.display.textEditor.Composer;
import trac.display.textEditor.CompositionPanel;
import trac.text.Word;

/**
 * Cette classe contruit la fenetre principale et place les differents
 * composants (JPanel) dans cette même fenetre
 */

public class Frame extends JFrame {

	private static final long serialVersionUID = 1L;

	private int width;
	private int height;

	private CompositionPanel compositionPan;
	private GraphicalPanel graphicalPan;

	private Set<Word> words;

	public Frame(int width, int height) {
		super("TRAC gr1 : Presentation IHM (Beta)");

		this.setSize(width, height);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		words = new HashSet<Word>();
		loadRessources();

		// création du panneau de composition
		compositionPan = new CompositionPanel(words, new Point(0, 0), new Dimension(400, this.getHeight()));
		compositionPan.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5, Color.LIGHT_GRAY));

		compositionPan.getButtonPanel().getEndLineButton()
				.addActionListener(new EndLineButtonListener());
		compositionPan.getButtonPanel().getAddWordButton()
				.addActionListener(new AddWordButtonListener());
		compositionPan.getButtonPanel().getEndTextButton()
				.addActionListener(new EndTextButtonListener());

		// creation du panneau d'affichage graphique
		graphicalPan = new GraphicalPanel(new Point(400, 0), new Dimension(this.getWidth() - 400, this.getHeight()));

		Container contentPane = this.getContentPane();
		contentPane.setLayout(null);// pas de layout manager pour les 2 Panel, on les place directement a partir des
//									// coordonées
		contentPane.add(compositionPan);
		contentPane.add(graphicalPan);

		this.revalidate();
		this.repaint();
	}

	// classe qui se declenche a l'appui sur le bouton
	class EndLineButtonListener implements ActionListener {
		String sentence;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int off;
			try {
				off = compositionPan.getEditorPanel().getTextEditor()
						.getLineStartOffset(compositionPan.getEditorPanel().getTextEditor().getLineCount() - 1);
				int end = compositionPan.getEditorPanel().getTextEditor()
						.getLineEndOffset(compositionPan.getEditorPanel().getTextEditor().getLineCount() - 1);

				sentence = compositionPan.getEditorPanel().getTextEditor().getText(off, end - off);
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// lancement du thread de composition
			new Composer(sentence, compositionPan.getSentencePanel(), graphicalPan, words).start();
		}
	}
	
	class EndTextButtonListener implements ActionListener {
		String sentence;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			graphicalPan.clearImage();
			graphicalPan.resetBackGround();
			compositionPan.getEditorPanel().getTextEditor().setText("");
			compositionPan.getSentencePanel().getSentenceLabel().setText("");
			new Player().stopPlaying();
		}
	}

	class AddWordButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new AddWordDialog(Frame.this, true, words);
			loadRessources();
		}
	}

	public void loadRessources() {
		File repertoire = new File("ressources");
		File liste[] = repertoire.listFiles();
		words.clear();

		if (liste != null) {
			for (int i = 0; i < liste.length; i++) {
				if (liste[i].isDirectory()) {
					words.add(loadWord(liste[i]));
				}
			}
		} else {
			System.err.println("Nom de repertoire invalide");
		}
	}

	private Word loadWord(File wordPath) {
		boolean isBackground = false;
		String imagePath = null;
		String songPath = null;

		String liste[] = wordPath.list();
		File attribut = new File(wordPath.getPath() + "/attribut.txt");
		String data = null;

		// ouverture du fichier attribut
//		for (int i = 0; i < liste.length; i++) {
//			
//			if (liste[i].startsWith("attribut.txt")) {
//				attribut = new File(wordPath.getPath() + "/attribut.txt");
//			}
//		}

		if (attribut.exists()) // on lit le fichier attribut
		{
			try {
				Scanner scan = new Scanner(attribut);
				data = scan.nextLine();
				scan.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (data.contains("false")) {
				isBackground = false;
			} else {
				isBackground = true;
			}

		} else { // sinon on le cree
			File attributCreate = new File(wordPath.getPath() + "/attribut.txt");
			try {
				attributCreate.createNewFile();
				FileWriter writ = new FileWriter(wordPath.getPath() + "/attribut.txt");
				writ.write("isBackground=false");
				writ.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// on recupere toutes les infos neccessaires a la cr�ation de l'objet Word
		for (int i = 0; i < liste.length; i++) {
			if (liste[i].contains(".png") || liste[i].contains(".gif") || liste[i].contains(".jpg")
					|| liste[i].contains(".PNG") || liste[i].contains(".GIF") || liste[i].contains(".JPG")) {
				imagePath = wordPath.getPath() + "/" + liste[i];

			}

			if (liste[i].contains(".wav") || liste[i].contains(".WAV")) {
				songPath = wordPath.getPath() + "/" + liste[i];
			}
		}
		// System.out.println(wordPath.getName() +"|"+ imagePath +"|"+ songPath +"|"+
		// isBackground);
		return new Word(wordPath.getName(), imagePath, songPath, isBackground);
	}
}

package trac.audio;


import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
 
/**
 * Classe permettant de lancer un fichier audio (.wav) dans un thread
 *
 *Si un son demande a etre lancé alors qu'un autre est en train d'etre jouer alors 
 *le morceau en cours s'arrete pour laisser la ligne libre pour le nouveau son
 */
public class Player extends Thread implements LineListener {
     
    /**
     * this flag indicates whether the playback completes or not.
     */
    private static boolean isPlaying = false;
    private String audioFilePath;
    
    public Player() {}
    public Player(String audioFilePath) {
    	super();
    	this.audioFilePath = audioFilePath;
    }
    
    public void stopPlaying() {
    	isPlaying = false;
    }
    /**
     * Play a given audio file.
     * @param audioFilePath Path of the audio file.
     */
    public void run() {
        File audioFile = new File(audioFilePath);
        System.out.println(audioFilePath+" lanc�e!");
 
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
 
            AudioFormat format = audioStream.getFormat();
 
            DataLine.Info info = new DataLine.Info(Clip.class, format);
 
            Clip audioClip = (Clip) AudioSystem.getLine(info);
 
            audioClip.addLineListener(this);
 
            audioClip.open(audioStream);
             
            audioClip.start();
            isPlaying = true;
             
            while (isPlaying) {
                // wait for the playback completes
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
             
            audioClip.close();
             
        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }
         
    }
     
    /**
     * Listens to the START and STOP events of the audio line.
     */
    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();
         
        if (type == LineEvent.Type.START) {
             
        } else if (type == LineEvent.Type.STOP) {
            isPlaying = false;
        }
 
    }

	public static boolean isPlaying() {
		return isPlaying;
	}

	public static void setPlaying(boolean isPlaying) {
		Player.isPlaying = isPlaying;
	} 
}
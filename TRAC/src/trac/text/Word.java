package trac.text;

public class Word {
	private boolean carriageReturn;
	private boolean endOfFile;
	private String word;
	private String trigger;
	
	public Word(String word, String trigger, boolean carriageReturn,boolean EOF) {
		super();
		this.carriageReturn = carriageReturn;
		this.endOfFile = EOF;
		this.word = word;
		this.trigger = trigger;
	}
	
	public Word() {
		super();
	}

	public boolean isCarriageReturn() {
		return carriageReturn;
	}

	public void setCarriageReturn(boolean carriageReturn) {
		this.carriageReturn = carriageReturn;
	}

	public boolean isEndOfFile() {
		return endOfFile;
	}

	public void setEndOfFile(boolean endOfFile) {
		this.endOfFile = endOfFile;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	@Override
	public String toString() {
		return "Word [carriageReturn=" + carriageReturn + ", endOfFile=" + endOfFile + "]";
	}

	
	
	

}

package trac;

import java.util.HashMap;
import java.util.Map;

import trac.audio.Player;
import trac.text.Reader;

public class Main {

	public static void main(String[] args) {
//		Player player = new Player();
//		player.play("/home/leo/Documents/ESISAR/4A/Projet art-science/banque de son/foret.wav");
//		player.play("/home/leo/Documents/ESISAR/4A/Projet art-science/banque de son/auto.wav");
		
		Map <String,String> triggerMap = new HashMap<String,String>();
		triggerMap.put("foret ", "./banque de son/foret.wav");
		triggerMap.put("ville ", "./banque de son/ville.wav");
		
		Reader reader = new Reader(triggerMap,"texte_babar.txt");
		reader.start();
	}
}
